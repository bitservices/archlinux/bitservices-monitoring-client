# Maintainer: Richard Lees <git0@bitservices.io>
###############################################################################

pkgname=bitservices-monitoring-client
pkgver=0.0.3
pkgrel=2
pkgdesc='Scrapes a Prometheus exporter and pushes the metrics to a Prometheus push gateway.'
arch=(x86_64)
url='https://gitlab.com/bitservices/archlinux/bitservices-monitoring-client'
install="${pkgname}.install"
license=('none')
makedepends=('go')
depends=('prometheus-node-exporter')
backup=("etc/${pkgname}.yaml")

source=("${pkgname}.service"
        "${pkgname}.sysusers"
        "${pkgname}.yaml"
        "config.go"
        "go.mod"
        "go.sum"
        "http.go"
        "main.go"
        "metrics.go")

sha256sums=('4c6939463fda555bfbca96660c8260a84fcbfb9e9500b368fc4bba6d585e607b'
            '61da3d97a4a1db09c0c3cab146a39445b6da6773db54556a003d71ab7e087d11'
            'a56522f8e141d5dce76de35a0191f04f4a7feaca36f3114e230ccb5e6bbafbbf'
            'e0d20e70be23d48eae1dd158bc0a2432d9898a21ac6370754320370d2f3561de'
            '56d1a87d1405b960feed9b00a2c01c1e96294e501659dcef6da104d4787a52f2'
            '379aed5b411605289d77f57e2873cb70f8398a8c5e2d86bca98f123d6c55b0b5'
            '25a40356c4f3f54a4e835a53ce0c6238d0de46c885514ad7ccc927187bd0ef61'
            '089d3f2724926f163a3d1d8db31b1f952b8a7da860ef0c6d962630a7cf996afb'
            '959687a3b787d7ba5696b67a20de45694ddbed90ad8a3c1e63e8ce123c9db2cd')

###############################################################################

build () {
  go build \
    -o="${pkgname}" \
    -mod=readonly \
    -modcacherw \
    -trimpath \
    .
}

###############################################################################

package() {
  install -Dm755 "${pkgname}" "${pkgdir}/usr/bin/${pkgname}"

  install -Dm644 "${srcdir}/${pkgname}.service" "${pkgdir}/usr/lib/systemd/system/${pkgname}.service"
  install -Dm644 "${srcdir}/${pkgname}.sysusers" "${pkgdir}/usr/lib/sysusers.d/${pkgname}.conf"

  install -Dm640 -g510 "${srcdir}/${pkgname}.yaml" "${pkgdir}/etc/${pkgname}.yaml"
}

###############################################################################
