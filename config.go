package main

import (
	"errors"
	"os"
	"slices"
	"time"

	"gopkg.in/yaml.v3"
)

type exporter struct {
	Host     string
	Port     int
	Protocol string
}

type gateway struct {
	Host       string
	Port       int
	Password   string
	Protocol   string
	Username   string
	ForceTLS13 bool
}

type config struct {
	Instance string
	Interval time.Duration
	Job      string
	Exporter *exporter
	Gateway  *gateway
}

func getConfig(configfile *string) (*config, error) {
	file, err := os.ReadFile(*configfile)

	if err != nil {
		return nil, err
	}

	config := &config{
		Interval: time.Duration(30 * time.Second),
		Job:      "push-gateway-node",

		Exporter: &exporter{
			Host:     "localhost",
			Port:     9115,
			Protocol: "http",
		},

		Gateway: &gateway{
			Port:       9091,
			Protocol:   "https",
			ForceTLS13: false,
		},
	}

	err = yaml.Unmarshal(file, &config)

	if err != nil {
		return nil, err
	}

	if config.Instance == "" || config.Job == "" || config.Exporter.Host == "" || config.Gateway.Host == "" {
		return nil, errors.New("Configuration file has missing mandatory values.")
	}

	if !slices.Contains([]string{"https", "http"}, config.Exporter.Protocol) {
		return nil, errors.New("Node exporter protocol must be HTTP or HTTPS!")
	}

	if !slices.Contains([]string{"https", "http"}, config.Gateway.Protocol) {
		return nil, errors.New("Push gateway protocol must be HTTP or HTTPS!")
	}

	return config, err
}
