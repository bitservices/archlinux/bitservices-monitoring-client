package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-co-op/gocron/v2"
)

func main() {
	// Read command line flags.
	configfile := flag.String("config", "", "The configuration file to use")
	flag.Parse()

	if *configfile == "" {
		fmt.Println("-config must be specified with a valid configuration file path")
		os.Exit(2)
	}

	// Read the configuration file.
	fmt.Println("Reading config: " + *configfile)
	config, err := getConfig(configfile)

	if err != nil {
		fmt.Println("Failed to read configuration: " + err.Error())
		os.Exit(1)
	}

	// Create a HTTP client
	httpClient := getClient(config.Gateway.ForceTLS13)

	// Create the job scheduler
	scheduler, err := gocron.NewScheduler()

	if err != nil {
		fmt.Println("Failed to start job scheduler!")
		os.Exit(1)
	}

	// Setup signal catcher BEFORE scheduling anything
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)

	go func() {
		signal := <-signals

		switch signal {
		case syscall.SIGINT:
			shutdown(scheduler, config, httpClient)
		case syscall.SIGQUIT:
			shutdown(scheduler, config, httpClient)
		case syscall.SIGTERM:
			shutdown(scheduler, config, httpClient)
		}
	}()

	// Schedule the metric forwarding job
	job, err := scheduler.NewJob(
		gocron.DurationJob(config.Interval),
		gocron.NewTask(
			func() {
				err := forward(config, httpClient)

				if err != nil {
					fmt.Println("Failed to forward metrics: " + err.Error())
				}
			},
		),
		gocron.WithSingletonMode(gocron.LimitModeReschedule),
	)

	if err != nil {
		fmt.Println("Failed to create scheduled job!")
		os.Exit(1)
	}

	fmt.Printf("Scheduled job ID: %x\n", job.ID())

	// Start the scheduler
	scheduler.Start()

	// Block the process
	select {}
}

func shutdown(scheduler gocron.Scheduler, config *config, httpClient *http.Client) {
	fmt.Println("Stopping timer...")
	err := scheduler.Shutdown()

	if err != nil {
		fmt.Println("Failed to shut down the timer! Skipping cleanup...")
		os.Exit(1)
	}

	fmt.Println("Deleting instance from push gateway...")
	err = delete(config, httpClient)

	if err != nil {
		fmt.Println("Failed to delete instace: " + err.Error())
	}

	fmt.Println("Closing any still open HTTP connections...")
	httpClient.CloseIdleConnections()

	fmt.Println("Shutting down...")
	os.Exit(0)
}
