package main

import (
	"crypto/tls"
	"net/http"
	"time"
)

func getClient(forcetls13 bool) *http.Client {
	var tlsVersion uint16 = tls.VersionTLS12

	if forcetls13 {
		tlsVersion = tls.VersionTLS13
	}

	client := &http.Client{
		Transport: &http.Transport{
			MaxIdleConns: 10,
			TLSClientConfig: &tls.Config{
				MinVersion: tlsVersion,
			},
		},
		Timeout: 10 * time.Second,
	}

	return client
}
