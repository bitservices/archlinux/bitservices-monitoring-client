package main

import (
	"errors"
	"io"
	"net/http"
	"strconv"
)

func forward(config *config, httpClient *http.Client) error {
	// Get current metrics from node exporter.
	exporterResponse, err := httpClient.Get(buildExporterUrl(config))

	if err != nil {
		return err
	}

	defer exporterResponse.Body.Close()

	if exporterResponse.StatusCode != 200 {
		return errors.New("HTTP status from node selector: " + exporterResponse.Status + " was not expected!")
	}

	// Post the metrics to the push gateway.
	pushGatewayRequest, err := buildPushGatewayRequest(config, "POST", exporterResponse.Body)

	if err != nil {
		return err
	}

	pushGatewayRequest.Header.Set("Content-Type", "text/plain")

	pushGatewayResponse, err := httpClient.Do(pushGatewayRequest)

	if err != nil {
		return err
	}

	defer pushGatewayResponse.Body.Close()
	io.Copy(io.Discard, pushGatewayResponse.Body)

	if pushGatewayResponse.StatusCode != 200 {
		return errors.New("HTTP status from push gateway: " + pushGatewayResponse.Status + " was not expected!")
	}

	// Return success
	return nil
}

func delete(config *config, httpClient *http.Client) error {
	// Delete the current instance from the push gateway.
	pushGatewayRequest, err := buildPushGatewayRequest(config, "DELETE", nil)

	if err != nil {
		return err
	}

	pushGatewayResponse, err := httpClient.Do(pushGatewayRequest)

	if err != nil {
		return err
	}

	defer pushGatewayResponse.Body.Close()
	io.Copy(io.Discard, pushGatewayResponse.Body)

	if pushGatewayResponse.StatusCode != 202 {
		return errors.New("HTTP status from push gateway: " + pushGatewayResponse.Status + " was not expected!")
	}

	// Return success
	return nil
}

func buildExporterUrl(config *config) string {
	return config.Exporter.Protocol + "://" + config.Exporter.Host + ":" + strconv.Itoa(config.Exporter.Port) + "/metrics"
}

func buildPushGatewayRequest(config *config, method string, body io.Reader) (*http.Request, error) {
	pushGatewayRequest, err := http.NewRequest(
		method,
		config.Gateway.Protocol+"://"+config.Gateway.Host+":"+strconv.Itoa(config.Gateway.Port)+"/metrics/job/"+config.Job+"/instance/"+config.Instance,
		body,
	)

	if err != nil {
		return nil, err
	}

	if config.Gateway.Username != "" && config.Gateway.Password != "" {
		pushGatewayRequest.SetBasicAuth(config.Gateway.Username, config.Gateway.Password)
	}

	return pushGatewayRequest, nil
}
